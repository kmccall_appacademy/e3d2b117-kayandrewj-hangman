class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @referee = players[:referee]
    @guesser = players[:guesser]
    @board = []
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(guess, indices)
    indices.each { |idx| @board[idx] = guess}
  end

  def over?
    board.count(nil) == 0
  end

  def conclude
    puts "You won!"
    p @secret_word
  end

end

class HumanPlayer

  def initialize
    @past_guesses = []
  end

  def pick_secret_word
    puts "Think of a word; enter it's length:"
    gets.chomp.to_i
  end

  def check_guess(guess)
    puts "The computer guessed #{guess}"
    puts "Positions: (1 3 5)"
    gets.chomp.split.map(&:to_i)

  end

  def guess(board)
    print_board(board)
    print "Guess a character: "
    gets.chomp
  end

  def handle_response(guess, indices)
    if @past_guesses.include?(guess)
      print "You've already guessed that character.\n"
      print "Guess again! You've already guessed #{@past_guesses}"
    else
      @past_guesses << guess
    end
  end

  def register_secret_length(length)
    puts "The length of the word is #{length} characters long."
  end

  private

  def print_board(board)
    board_string = board.map { |char| char.nil? ? "_" : char}.join("")
    p board_string
  end

end

class ComputerPlayer
  def self.read_dictionary
    dictionary = []
    File.readlines('dictionary.txt') { |line| dictionary << line}
  end

  attr_accessor :dictionary

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ('a'..'z').to_a
  end

  def pick_secret_word
    p @secret_word = @dictionary.sample.chomp
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |char, idx|
      indices << idx if char == letter
    end
    indices
  end

  def register_secret_length(length)
    dictionary.select! {|word| word.length == length}
  end

  def guess(board)
    guess = @alphabet.sample
    @alphabet.delete(guess)
    guess
  end

  def handle_response(guess, indices)
    @dictionary.select! do |word|
      word_indices = []
      word.chars.each_with_index do |char, idx|
        word_indices << idx if char == guess
      end
      indices == word_indices
    end
  end

  def candidate_words

  end

end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    guesser: ComputerPlayer.new(dictionary),
    referee: HumanPlayer.new

  }
  game = Hangman.new(players)
  game.play
end
